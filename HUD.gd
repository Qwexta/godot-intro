extends CanvasLayer
signal start_game

func show_message(text):
	$MessageLabel.text = text
	$MessageLabel.show()
	$Timer.start()

func show_game_over():
	show_message("Game Over")
	yield($Timer, "timeout")
	$MessageLabel.text = "Dodge the Creeps!"
	$MessageLabel.show()
	yield(get_tree().create_timer(1), 'timeout')
	$Button.show()

func _ready():	
	pass # Replace with function body.

func update_score(score):
	$ScoreLabel.text = str(score)

func _on_Button_pressed():
	$Button.hide()
	emit_signal("start_game")

func _on_Timer_timeout():
	$MessageLabel.hide()
