extends RigidBody2D

export var min_speed = 150
export var max_speed = 250
var mob_type = ["fly", "walk", "swim"]

func _ready():
	$AnimatedSprite.animation = mob_type[randi() % mob_type.size()]
	$AnimatedSprite.play()

func _on_VisibilityNotifier2D_viewport_exited(v):
	queue_free()
